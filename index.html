<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            Using VSCode with UW CS Linux Servers
        </title>
        <style>
          .codeblock {
            border: 1px solid;
            padding: 10px;
          }
        </style>
    </head>
    <body>
        <h1>Using VSCode with UW CS Linux Servers</h1>

        <h2>Installing "Remote - SSH" extension</h2>
        <p>
            Watch the <a href="https://code.visualstudio.com/learn/get-started/extensions">VS Code Extensions video</a> and install the "Remote - SSH" extension by Microsoft.
        </p>

        <h2>Setting up the "Remote - SSH" extension</h2>

        <p>
            Click the green <img src="vscode_remote_connect_icon.png" alt="VSCode Remote Connect Icon" width="33" height="25" /> icon at bottom left of VSCode, and choose "Connect to Host". Then click "Configure SSH Hosts" and
            choose the first option, which will vary depending on if your computer is Mac (~/.ssh/config) or Windows (C:\Users\username\.ssh\config). A sample config file is below
            (replace userid with your UW username, up to 8 characters):
        </p>

        <pre class="codeblock">
Host *.student.cs.uwaterloo.ca
  # Replace userid with your UW username
  User userid
  # Optional: If you have an SSH key generated on your computer,
  # you can specify its location by uncommenting the following line:
  # (by default, it looks in your .ssh folder for an id_rsa file)
  # IdentityFile ~/.ssh/id_rsa

Host ubuntu2204-002.student.cs.uwaterloo.ca

Host ubuntu2204-004.student.cs.uwaterloo.ca

Host ubuntu2204-006.student.cs.uwaterloo.ca

Host ubuntu2204-010.student.cs.uwaterloo.ca

Host ubuntu2204-012.student.cs.uwaterloo.ca

Host ubuntu2204-014.student.cs.uwaterloo.ca
</pre>

        <p> This sample SSH config file will make VSCode let you choose which Linux server to use when you click "Connect to Host". An up-to-date list of available servers is
            on the <a href="https://uwaterloo.ca/computer-science-computing-facility/teaching-hosts">CSCF teaching hosts web page</a>.</p>

        <p>If the list of servers on that web page change, you will have to update the SSH config on your computer yourself.</p>

        <p>
            If you have SSH keys generated, you can use the <tt>IdentityFile</tt> setting to tell VSCode
            where your <tt>id_rsa</tt> file is. Alternatively, you can type in your password each time,
            but be aware VSCode can ask you for your password a lot. It will be even more annoying 
            from off-campus without VPN, because you'll have to accept the 2FA prompt each time. For your own convenience,
            you may want to set up SSH keys.
        </p>
        
        <p>The following are instructions for generating your SSH keys:
        </p>
        
        <pre class="codeblock">
# Navigate to your ~/.ssh folder on your own computer
# WINDOWS:  cd \Users\your_windows_username\.ssh
# MAC:      cd ~/.ssh

# Generate your keys
ssh-keygen
(then press [enter] [enter] [enter] if you do not wish to have a passphrase)

# copy your id_rsa.pub file to the school
scp id_rsa.pub username@linux.student.cs.uwaterloo.ca:.ssh/my_pub

# Now, log into the school
ssh username@linux.student.cs.uwaterloo.ca

# append your my_pub file to your authorized_keys file (it may or may not exist)
cd .ssh
cat my_pub &gt;&gt; authorized_keys
</pre>

        <h3>Which server should I use? linux.student.cs or ubuntu2204-NNN and what NNN?</h3>

        <p>You should be using ubuntu2204-NNN with VSCode. The Ubuntu 22.04 Linux servers currently available are listed on the
           <a href="https://uwaterloo.ca/computer-science-computing-facility/teaching-hosts">CSCF teaching hosts web page</a>.
        </p>

        <p>
           You can pick whichever one of the Linux servers you want, for example you might pick ubuntu2204-006.student.cs.uwaterloo.ca. <strong>But once you pick a server, try to keep
            using it each time you connect in VSCode.</strong> This will help avoid creating extra VSCode instances on other servers, which takes up extra computing resources (CPU, RAM/memory).
            When you close VSCode on your computer and re-open it later, VSCode automatically connects to the same server you were using before, so you don't need to remember it yourself.
        </p>

        <p>
            If the server that you picked is not working, then you can use another server. To do this, save your work then <a href="#closing_remote_connection">close your VSCode remote connection</a>. Then connect to another server.
            With the sample SSH config above, after you click "Connect to Host" in VSCode, you will
            see a drop-down menu of the servers in the config. You can pick a different one from your usual server. Any of the Linux servers may go down for various reasons,
            such as crash or hardware failure, but I.T. staff will try to fix it as quickly as possible. With multiple servers, even if 1 or 2 is down, you should still be able to connect to the other
            ones while the broken servers are being repaired.
        </p>

        <p>
            Having multiple VSCode windows open and connected remotely will use more resources, so avoid that if possible.
        </p>

        <h4>Explanation of avoiding "linux.student.cs.uwaterloo.ca" with VSCode</h4>
        <p>Using "linux.student.cs.uwaterloo.ca" will randomly redirect you to one of the
            Linux servers, and the server you randomly get can change often. VSCode does not always shut itself down properly on the server.
            Using VSCode with "linux.student.cs.uwaterloo.ca" can take up computing
            resources on multiple servers, even though you really only need to be using just one server. As an analogy, it'd be like taking up
            multiple seats in the classroom when you only need one seat.
        </p>
            
         <p>If you are connecting with simpler SSH programs like <a href="https://www.putty.org/">PuTTY</a> or SSH command in Mac Terminal, then
            you can use "linux.student.cs.uwaterloo.ca" because these programs use fewer computing resources than VSCode, and they shut
            themselves down faster on the server when you close them.
         </p>


        <h2>Extensions and C/C++ IntelliSense</h2>
        <p>
            Please avoid installing VSCode extensions while connected remotely, unless the course tells you to. For CS136(L), the only extensions
            you should have installed remotely are the <a href="https://student.cs.uwaterloo.ca/~cs_build/marmoset-vscode/">Marmoset VSCode extension</a>
            and C/C++ extension from Microsoft (see CS136L for more info on this extension). The reason for this rule is because some VSCode extensions 
            use a ridiculous amount of CPU and memory, and they make  the server unusable for other
            students. If you install these kind of extensions, and the I.T. staff detect that your extension is causing problems on the
            server, the extension will be terminated without warning.
        </p>

        <p>The C/C++ IntelliSense extension sometimes use a large amount of memory (several gigabytes). To stop it from doing this,
            open the VSCode settings (gear icon at bottom left). Then search for "memory" and click the "Remote [SSH...]" tab. Several
            settings related to memory will appear. Set the "C_Cpp: Max Memory" and "C_Cpp: Intelli Sense Memory Limit" to the minimum of 256.
        </p>

        <img src="c_cpp_intellisense_settings.png" alt="Configuring VSCode C/Cpp IntelliSense Memory Limit" />

        <h2 id="closing_remote_connection">Closing VSCode Remote Connection</h2>

        <p>First, remember to save all your work. Then when you're done using VSCode, or if you want to switch servers because the server you're on is broken, go to File =&gt;
            Close Remote Connection. Then close the VSCode window. If you want to reconnect, click the remote connection icon
            <img src="vscode_remote_connect_icon.png" alt="VSCode Remote Connect Icon" width="33" height="25" />.
        </p>

        <h2>Connecting Remotely after updating VSCode</h2>
        <p>Microsoft releases VSCode updates at least once a month. After you update VSCode, the next time you connect to the Linux servers will take a while.
            The reason is that VSCode tries to delete a large folder in your Linux account, and this deletion can take a few minutes. Installing the new VSCode files
            into your account can take some time, too. You may have to retry connecting a couple times after a VSCode update.
        </p>

        <h2>Other questions</h2>

        <ul>
            <li><b>How do I tell which servers are up or down?</b> You can check the <a href="http://icinga.cscf.uwaterloo.ca/grafana/goto/PqhskAKIg?orgId=1">Linux teaching servers status web page</a>.
            </li>

            <li><b>Do my SSH keys need a passphrase?</b> This is up to you. For your own security (for example you're worried your laptop
                may get stolen or hacked), you can add a passphrase when generating your SSH keys.
            </li>

            <li><b>The server I'm working on suddenly broke/crashed, and I cannot save my files. What should I do?</b> VSCode should show a small
                error window. Click Save As, then Show Local. Then you can save your work on your own computer as a backup copy until you reconnect
                to another working server. To copy files from your computer to the Linux server, you can drag-and-drop the files from
                Finder (Mac) or Explorer (Windows) into VSCode's File Explorer sidebar.
                <br/><br/><img src="vscode_save_failed1.png" alt="VSCode Failed to Save, click Save As button" />
                <br/><br/><img src="vscode_save_failed2.png" alt="VSCode Failed to Save, click Show Local to save locally" />
            </li>
        </ul>
    </body>
</html>
